Kubernetes Deployment patterns
---

With Kubernetes, it is possible to utilize a number of different deployment
patterns that can increase the availability of your application.

### [Canary](https://martinfowler.com/bliki/CanaryRelease.html)

deploy v1 with 4 replicas:
```
k apply -f canary-ingress.yml
k apply -f canary-v1.yml
```
** if on dks use canary-ingress.dks.yml

deploy the v2 with 1 replica:
```
k apply -f canary-v2.yml
```

There are now 4 stable and 1 canary pods that are selected by the canary-svc selector.
It will load balance 4/5 requests to v1, and 1/5 requests to v2.

If the canary is acceptable, scale the app-canary deployment to 4 and delete the canary-v1 deployment 
```
k scale deploy/app-v2 --replicas=4
k delete deploy/app-v1
```

If the canary is not acceptable, delete it
```
k delete deploy/app-v2
```

### [Blue - Green](https://martinfowler.com/bliki/BlueGreenDeployment.html)

#### Option 1. Ingress Based
deploy Blue:
```
k apply -f bg-ingress.yml
k apply -f bg-blue-deploy.yml
k apply -f bg-blue-svc.yml
curl -s bgapp.oasis.localhost | grep Hostname
```

deploy Green:
```
k apply -f bg-green-ingress.yml
k apply -f bg-green-deploy.yml
k apply -f bg-green-svc.yml
```

test Green using bg-green-ingress
```
curl -s bgapp-green.oasis.localhost | grep Hostname
```

if test passes, replace bg-ingress with new version pointing to bg-green-svc, decommission bg-green-ingress
```
k apply -f bg-ingress-pass.yml
k delete -f bg-green-ingress.yml
curl -s bgapp.oasis.localhost | grep Hostname
```

option 1: leave bg-blue-svc and bg-blue-deploy around for a while, so that
if things go south you can rollback:
```
k apply -f bg-ingress-rollback.yml
curl -s bgapp.oasis.localhost | grep Hostname
```

option 2: decommission bg-blue
```
k delete -f bg-blue-svc.yml
k delete -f bg-blue-deploy.yml
```

if test fails, decommission bg-green-ingress, bg-green-svc, bg-green-deploy
```
k delete -f bg-green-ingress.yml
k delete -f bg-green-svc.yml
k delete -f bg-green-deploy.yml
```

#### Option 2. Service Based
deploy Blue:
```
k apply -f bg2-ingress.yml
k apply -f bg2-blue-svc.yml
k apply -f bg-blue-deploy.yml
```

deploy Green:
```
k apply -f bg-green-ingress.yml
k apply -f bg-green-svc.yml
k apply -f bg-green-deploy.yml
curl -s bgapp.oasis.localhost | grep Hostname
```
* note that bg-green-ingress, bg-green-svc, bg-blue-deploy and bg-green-deploy are reused

test Green using bg-green-ingress
```
curl -s bgapp-green.oasis.localhost | grep Hostname
```

if test passes, replace bg-service with new version pointing to bg-green-deploy, decommission bg-green-ingress and bg-green-service
```
k apply -f bg2-svc-pass.yml
k delete -f bg-green-ingress.yml
k delete -f bg-green-svc.yml
curl -s bgapp.oasis.localhost | grep Hostname
```

option 1: leave bg-blue-deploy around for a while, so that
if things go south you can rollback:
```
k apply -f bg2-svc-rollback.yml
curl -s bgapp.oasis.localhost | grep Hostname
```

option 2: decommission bg-blue
```
k delete -f bg-blue-deploy.yml
```

if test fails, decommission bg-green-ingress, bg-green-svc, bg-green-deploy
```
k delete -f bg-green-ingress.yml
k delete -f bg-green-svc.yml
k delete -f bg-green-deploy.yml
```

#### Option 3. Pod Label Based

deploy Blue:
```
k apply -f bg3-ingress.yml
k apply -f bg3-blue-deploy.yml
curl -s bgapp.oasis.localhost | grep Hostname
```

deploy Green:
```
k apply -f bg-green-ingress.yml
k apply -f bg-green-svc.yml
k apply -f bg3-green-deploy.yml
curl -s bgapp.oasis.localhost | grep Hostname
```
* note that bg-green-ingress and bg-green-svc are reused

test Green using bg-green-ingress
```
curl -s bgapp-green.oasis.localhost | grep Hostname
```

If test passes, removed accepted: true label from pods from bg-v1-deploy,
and add it to pods from bg-v2-deploy
```
k label pod -l app=bluegreen,version=v1 accepted-
k label pod -l app=bluegreen,version=v2 accepted="true"
k delete -f bg-green-ingress.yml
k delete -f bg-green-svc.yml
curl -s bgapp.oasis.localhost | grep Hostname
```

option 1: leave bg-blue-deploy around for a while, so that
if things go south you can rollback:
```
k label pod -l app=bluegreen,version=v2 accepted-
k label pod -l app=bluegreen,version=v1 accepted="true"
curl -s bgapp.oasis.localhost | grep Hostname
```

option 2: decommission deploy/bg-v1-deploy
```
k delete deploy/bg-v1-deploy
```

If test fails, remove deploy/bg-v2-deploy
```
k delete deploy/bg-v2-deploy
k delete -f bg-green-ingress.yml
k delete -f bg-green-svc.yml
```

One downside to the label based option is that it cannot be done by
applying an update to existing objects using the declarative method, it must
be done with imperative kubectl commands.

All options could be automated with CI or Operators.